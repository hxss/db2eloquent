<?php

namespace DB2Eloquent;

use DB2Eloquent\Traits\StaticSinglton;
use Doctrine\Common\Inflector\Inflector as DoctrineInflector;

class Inflector extends DoctrineInflector {

	use StaticSinglton;
}
