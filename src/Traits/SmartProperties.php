<?php

namespace DB2Eloquent\Traits;

trait SmartProperties {

	public function __get($name) {
		$methodName = 'get' . ucfirst($name);

		if (method_exists($this, $methodName)) {
			return $this->$methodName();
		}
	}

	public function __set($name, $val) {
		$methodName = 'set' . ucfirst($name);

		if (method_exists($this, $methodName)) {
			return $this->$methodName($val);
		}
	}
}
