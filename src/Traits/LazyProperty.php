<?php

namespace DB2Eloquent\Traits;

trait LazyProperty {

	protected function lazyProp($name, $fallback) {
		if ($this->$name === null) {
			$this->$name = call_user_func($fallback);
		}

		return $this->$name;
	}
}
