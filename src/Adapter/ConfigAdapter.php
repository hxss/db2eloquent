<?php

namespace DB2Eloquent\Adapter;

class ConfigAdapter extends Adapter {

	const SRC_DB = 'db';
	const SRC_NAMESPACE = 'namespace';
	const SRC_PATH = 'path';
	const SRC_FILTER = 'filter';

	protected $_db = null;
	protected $_namespace = null;
	protected $_path = null;

	protected $_includeList = null;
	protected $_includeRegex = null;
	protected $_excludeList = null;
	protected $_excludeRegex = null;

	protected $_rootPath = null;

	protected $_filter = null;

	public function getDb() {
		return $this->dataPropProxy('_db', static::SRC_DB);
	}

	public function getNamespace() {
		return $this->dataPropProxy('_namespace', static::SRC_NAMESPACE);
	}

	public function getPath() {
		return $this->dataPropProxy('_path', static::SRC_PATH);
	}

	public function getRootPath() {
		return $this->lazyProp('_rootPath', function() {
			return dirname(dirname(__DIR__));
		});
	}

	public function getIncludeList() {
		return $this->lazyProp('_includeList', function() {
			return collect(
				isset($this->getFilter()['include']['list'])
					? $this->getFilter()['include']['list']
					: []
			);
		});
	}

	public function getIncludeRegex() {
		return $this->lazyProp('_includeRegex', function() {
			return isset($this->getFilter()['include']['regex'])
				? $this->getFilter()['include']['regex']
				: null;
		});
	}

	public function getExcludeList() {
		return $this->lazyProp('_excludeList', function() {
			return collect(
				isset($this->getFilter()['exclude']['list'])
					? $this->getFilter()['exclude']['list']
					: []
			);
		});
	}

	public function getExcludeRegex() {
		return $this->lazyProp('_excludeRegex', function() {
			return isset($this->getFilter()['exclude']['regex'])
				? $this->getFilter()['exclude']['regex']
				: null;
		});
	}

	protected  function getFilter() {
		return $this->dataPropProxy('_filter', static::SRC_FILTER);
	}
}
