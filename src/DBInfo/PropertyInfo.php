<?php

namespace DB2Eloquent\DBInfo;

use DB2Eloquent\DBInfo\Postgresql\ColumnInfo;

class PropertyInfo {

	protected $name = null;
	protected $constant = null;

	public function __construct(ColumnInfo $column) {
		$this->name = camel_case($column->getName());
		$this->constant = 'PROP_' . strtoupper($column->getName());
	}

	public function getName() {
		return $this->name;
	}

	public function getConstant() {
		return $this->constant;
	}
}
