<?php

namespace DB2Eloquent\DBInfo\Postgresql;

use DB2Eloquent\Interfaces\InterfaceContext;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Collection;

class ModelsList extends Collection {

	public static function create(InterfaceContext $context) {
		$list = new static();

		$tables = Capsule::query()
			->select([
				't.table_schema',
				't.table_name',
			])
			->from('information_schema.tables as t')
			->where([
				'table_catalog' => Capsule::query()
					->selectRaw('current_database()')
					->value('current_database'),
				'table_type' => 'BASE TABLE'
			])
			->whereNotIn(
				'table_schema',
				['pg_catalog', 'information_schema']
			)
			->get();

		$tables->each(function($table) use($list, $context) {
			$modelInfo = new ModelInfo(
				$context,
				$table->table_name,
				$table->table_schema
			);

			$list[$modelInfo->getFullTableName()] = $modelInfo;
		});

		return $list;
	}
}
