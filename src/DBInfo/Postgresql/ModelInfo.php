<?php

namespace DB2Eloquent\DBInfo\Postgresql;

use DB2Eloquent\Command\GenerateModelsCommand;
use DB2Eloquent\DBInfo\PropertyInfo;
use DB2Eloquent\Generator\ModelGenerator;
use DB2Eloquent\Inflector;
use DB2Eloquent\Interfaces\InterfaceContext;
use DB2Eloquent\Link\Link;
use DB2Eloquent\Link\ManyToManyLink;
use DB2Eloquent\Traits\LazyProperty;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Collection;

class ModelInfo {

	use LazyProperty;

	const SCHEMA_PUBLIC = 'public';

	protected $context = null;

	protected $table = '';
	protected $schema = '';

	protected $pkInfo = null;
	protected $pk = null;
	protected $incrementing = null;

	protected $columns = null;

	protected $fullBaseClassName = null;
	protected $fullClassName = null;
	protected $className = null;
	protected $fullBaseNamespace = null;
	protected $fullNamespace = null;
	protected $namespace = null;

	protected $properties = null;

	protected $links = null;
	protected $isPivot = null;
	protected $manyToManyLinks = null;

	protected $isIncluded = null;

	public function __construct(InterfaceContext $context, $table, $schema) {
		$this->table = $table;
		$this->schema = $schema;
		$this->context = $context;
	}

	public function isIncluded() {
		return $this->lazyProp('isIncluded', function() {
			$config = $this->context->getConfig();
			$name = $this->getFullTableName();

			return (
				(
					$config->getIncludeList()->count()
					&& $config->getIncludeList()->contains($name)
					|| $config->getIncludeRegex()
					&& preg_match($config->getIncludeRegex(), $name)
					|| !$config->getIncludeList()->count()
					&& !$config->getIncludeRegex()
				)
				&& !(
					$config->getExcludeList()->count()
					&& $config->getExcludeList()->contains($name)
					|| $config->getExcludeRegex()
					&& preg_match($config->getExcludeRegex(), $name)
				)
			);
		});
	}

	public function getCollection() {
		return $this->collection;
	}

	public function getFullTableName() {
		return $this->getSchema() . '.' . $this->getTable();
	}

	public function getTable() {
		return $this->table;
	}

	public function getSchema() {
		return $this->schema;
	}

	public function getProperties() {
		return $this->lazyProp('properties', function() {
			return $this->getColumns()
				->map(function(ColumnInfo $column) {
					return new PropertyInfo($column);
				});
		});
	}

	public function getColumns() {
		return $this->lazyProp('columns', function() {
			$columns = Capsule::query()
				->select('a.attname as name')
				->selectRaw('col_description(c.oid, a.attnum) as "comment"')
				->from('pg_attribute as a')
				->join('pg_class as c', 'c.oid', '=', 'a.attrelid')
				->join('pg_namespace as n', 'n.oid', '=', 'c.relnamespace')
				->where([
					'n.nspname' => $this->getSchema(),
					'c.relname' => $this->getTable(),
					'a.attisdropped' => false,
				])
				->where('a.attnum', '>', 0)
				->get()
				->keyBy('name');

			$columns = $columns->map(function($column) {
				return new ColumnInfo($column->name, $column->comment);
			});

			return $columns;
		});
	}

	public function getPk() {
		return $this->lazyProp('pk', function() {
			return $this->getPkInfo()->pk;
		});
	}

	public function getIncrementing() {
		return $this->lazyProp('incrementing', function() {
			return $this->getPkInfo()->incrementing;
		});
	}

	public function isPivot() {
		return $this->lazyProp('isPivot', function() {
			return $this->getLinks()
				->filter(function($link) {
					return !$link->isParent();
				})
				->count() > 1;
		});
	}

	public function getLinks() {
		return $this->lazyProp('links', function() {
			$sql = file_get_contents(
				$this->context->getConfig()->rootPath . '/sql/table_links.sql'
			);

			$links = Collection::make(
				Capsule::select(
					$sql,
					[
						'schema' => $this->getSchema(),
						'table' => $this->getTable(),
					]
				)
			)->map([$this, 'createLink']);

			return $links->merge(
				$links->map([$this, 'reflectSelfLink'])
					->filter()
			);
		});
	}

	public function createLink($linkInfo) {
		$isParent = $linkInfo->parent_schema == $this->getSchema()
			&& $linkInfo->parent_table == $this->getTable();

		$linkInfo->parent_attrs = json_decode($linkInfo->parent_attrs);
		$linkInfo->child_attrs = json_decode($linkInfo->child_attrs);

		$link = $isParent
			? new Link(
				$linkInfo->parent_attrs,
				$linkInfo->child_schema . '.' . $linkInfo->child_table,
				$linkInfo->child_attrs,
				true
			)
			: new Link(
				$linkInfo->child_attrs,
				$linkInfo->parent_schema . '.' . $linkInfo->parent_table,
				$linkInfo->parent_attrs,
				false
			);

		return $link;
	}

	public function reflectSelfLink(Link $link) {
		return $link->getForeignTable() == $this->getFullTableName()
			? new Link(
				$link->getForeignKeys(),
				$link->getForeignTable(),
				$link->getKeys(),
				!$link->isParent()
			)
			: null;
	}

	public function getManyToManyLinks() {
		return $this->lazyProp('manyToManyLinks', function() {
			$m2mLinks = new Collection();
			$modelsList = $this->context->getModelsList();

			foreach ($this->getLinks() as $link) {
				if (
					($foreignClass = $modelsList[$link->getForeignTable()])
					&& $foreignClass->isPivot()
				) {
					$foreignLinks = $foreignClass->getLinks()->filter(
						function ($link) {
							return !$link->isParent()
								&& $link->getForeignTable() !== $this->getFullTableName();
						}
					);

					foreach ($foreignLinks as $foreignLink) {
						$m2mLinks[] = new ManyToManyLink(
							$link->getForeignKeys(),
							$foreignLink->getForeignTable(),
							$foreignLink->getKeys(),
							$link->getForeignTable()
						);
					}
				}
			}

			return $m2mLinks;
		});
	}

	protected function getPkInfo() {
		return $this->lazyProp('pkInfo', function() {
			$pkInfo = Capsule::query()
				->select([
					'pg_attribute.attname as pk',
					'pg_attribute.atthasdef as incrementing'
				])
				->from('pg_index')
				->join('pg_attribute', function($join) {
					$join->on('pg_attribute.attrelid', '=', 'pg_index.indrelid')
						->whereRaw('pg_attribute.attnum = any(pg_index.indkey)');
				})
				->join('pg_class', 'pg_class.oid', '=', 'pg_attribute.attrelid')
				->join(
					'pg_namespace',
					'pg_namespace.oid', '=', 'pg_class.relnamespace'
				)
				->where([
					'pg_index.indisprimary' => true,
					'pg_namespace.nspname' => $this->getSchema(),
				])
				->whereRaw(
					"pg_index.indrelid = ?::regclass",
					[$this->getFullTableName()]
				)
				->limit(1)
				->get();

			return $pkInfo->count()
				? $pkInfo[0]
				: (object)[
					'pk' => '',
					'incrementing' => true,
				];
		});
	}

	public function getFullBaseClassName() {
		return $this->lazyProp('fullBaseClassName', function() {
			return ModelGenerator::concatNamespace(
				$this->getFullBaseNamespace(),
				$this->getClassName()
			);
		});
	}

	public function getFullClassName() {
		return $this->lazyProp('fullClassName', function() {
			return ModelGenerator::concatNamespace(
				$this->getFullNamespace(),
				$this->getClassName()
			);
		});
	}

	public function getClassName() {
		return $this->lazyProp('className', function() {
			return static::classify($this->table);
		});
	}

	public function getFullBaseNamespace() {
		return $this->lazyProp('fullBaseNamespace', function() {
			return ModelGenerator::concatNamespace(
				$this->getFullNamespace(),
				GenerateModelsCommand::NAMESPACE_BASE
			);
		});
	}

	public function getFullNamespace() {
		return $this->lazyProp('fullNamespace', function() {
			return ModelGenerator::concatNamespace(
				$this->context->getConfig()->namespace,
				$this->getNamespace()
			);
		});
	}

	public function getNamespace() {
		return $this->lazyProp('namespace', function() {
			return static::classify(
				$this->schema === static::SCHEMA_PUBLIC
					? ''
					: $this->schema
			);
		});
	}

	static protected function classify($word) {
		return Inflector::get()->classify(
			implode(
				' ',
				array_map(function($word) {
					return Inflector::get()->singularize($word);
				}, preg_split('/[-_]/', $word))
			)
		);
	}
}
