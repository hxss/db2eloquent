<?php

namespace DB2Eloquent\DBInfo\Postgresql;

class ColumnInfo {

	protected $name = null;
	protected $comment = null;
	protected $constant = null;

	public function __construct($name, $comment) {
		$this->name = $name;
		$this->comment = $comment;
		$this->constant = 'COL_' . strtoupper($name);
	}

	public function getName() {
		return $this->name;
	}

	public function getComment() {
		return $this->comment;
	}

	public function getConstant() {
		return $this->constant;
	}
}
