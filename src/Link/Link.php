<?php

namespace DB2Eloquent\Link;

class Link {

	protected $keys = null;
	protected $foreignTable = null;
	protected $foreignKeys = null;
	protected $isParent = false;

	public function __construct($keys, $foreignTable, $foreignKeys, $isParent) {
		$this->keys = $keys;
		$this->foreignTable = $foreignTable;
		$this->foreignKeys = $foreignKeys;
		$this->isParent = $isParent;
	}

	public function getKeys() {
		return $this->keys;
	}

	public function getForeignTable() {
		return $this->foreignTable;
	}

	public function getForeignKeys() {
		return $this->foreignKeys;
	}

	public function isParent() {
		return $this->isParent;
	}
}
