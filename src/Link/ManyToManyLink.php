<?php

namespace DB2Eloquent\Link;

class ManyToManyLink extends Link {

	protected $pivotTable = null;

	public function __construct($keys, $foreignTable, $foreignKeys, $pivotTable) {
		parent::__construct($keys, $foreignTable, $foreignKeys, true);

		$this->pivotTable = $pivotTable;
	}

	public function getPivotTable() {
		return $this->pivotTable;
	}
}
