<?php

namespace DB2Eloquent\Generator;

use DB2Eloquent\DBInfo\Postgresql\ModelInfo;

class OneToManyMethodGenerator extends RelationMethodGenerator {

	const BODY_HAS_MANY = "return \$this->hasMany(\n\t%s::class,\n\t%s,\n\t%s\n);";
	const BODY_BELONGS_TO = "return \$this->belongsTo(\n\t%s::class,\n\t%s,\n\t%s\n);";

	public function generate() {
		if ($this->getForeignModel()) {

			($this->getForeignModel() != $this->modelInfo)
				&& $this->uses[] = [
					$this->getForeignModel()->getFullClassName(),
					$this->getChildAlias($this->getForeignModel())
				];

			$this->getMethod()
				->setBody($this->getBody());

			return $this->getMethod();
		}

		return null;
	}

	public function __toString() {
		return $this->modelInfo->getTable()
			. '.' . $this->link->getKeys()[0]
			. ' ~ '
			. $this->getForeignModel()->getTable()
			. '.' . $this->link->getForeignKeys()[0];
	}

	protected function getBody() {
		return $this->link->isParent()
			? sprintf(
				static::BODY_HAS_MANY,
				$this->getChildAlias($this->getForeignModel()),
				$this->getForeignKey(),
				$this->getKey()
			)
			: sprintf(
				static::BODY_BELONGS_TO,
				$this->getChildAlias($this->getForeignModel()),
				$this->getKey(),
				$this->getForeignKey()
			);
	}

	protected function getKey() {
		return $this->lazyProp('key', function() {
			return $this->getRelationKey(
				$this->modelInfo,
				$this->link->getKeys()[0]
			);
		});
	}

	protected function getForeignKey() {
		return $this->lazyProp('foreignKey', function() {
			return $this->getRelationKey(
				$this->getForeignModel(),
				$this->link->getForeignKeys()[0]
			);
		});
	}

	protected function getFqMethodName() {
		return $this->lazyProp('fqMethodName', function() {
			$fk = $this->link->isParent()
				? $this->getForeignModel()->getProperties()[
					$this->link->getForeignKeys()[0]
				]
				: $this->modelInfo->getProperties()[
					$this->link->getKeys()[0]
				];

			return $this->getMethodName()
				. 'By' . ucfirst($fk->getName());
		});
	}
}
