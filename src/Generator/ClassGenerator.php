<?php

namespace DB2Eloquent\Generator;

use Zend\Code\Generator\ClassGenerator as ZendModelGenerator;
use Zend\Code\Generator\MethodGenerator;
use Zend\Code\Generator\PropertyGenerator;

class ClassGenerator extends ZendModelGenerator {

	protected $egyptianBrackets = false;

	public function generate() {
		$output = parent::generate();

		if ($this->egyptianBrackets) {
			$output = static::convertToEgyptianBrackets($output);
		}

		$output = $this->fixExtendsAliasClassName($output);
		$output = $this->fixDocBlockIndentation($output);

		return $output;
	}

	public function setEgyptianBrackets($egyptianBrackets) {
		$this->egyptianBrackets = $egyptianBrackets;

		return $this;
	}

	public function getEgyptianBrackets() {
		return $this->egyptianBrackets;
	}

	public function addMethodFromGenerator(MethodGenerator $method) {
		$result = parent::addMethodFromGenerator($method);

		$method->setIndentation($this->getIndentation());
		$method->getDocBlock()
			&& $method->getDocBlock()->setIndentation($this->getIndentation());

		return $result;
	}

	public function addPropertyFromGenerator(PropertyGenerator $property) {
		$result = parent::addPropertyFromGenerator($property);

		$property->setIndentation($this->getIndentation());
		$property->getDocBlock()
			&& $property->getDocBlock()->setIndentation($this->getIndentation());

		return $result;
	}

	public function addConstantFromGenerator(PropertyGenerator $constant) {
		$result = parent::addConstantFromGenerator($constant);

		$constant->setIndentation($this->getIndentation());
		$constant->getDocBlock()
			&& $constant->getDocBlock()->setIndentation($this->getIndentation());

		return $result;
	}

	static private function convertToEgyptianBrackets($str) {
		return preg_replace(
			'/\n\s*{/m',
			' {',
			$str
		);
	}

	private function fixDocBlockIndentation($output) {
		return preg_replace('/(?<=^|\s)( {4})/', "\t", $output);
	}

	private function fixExtendsAliasClassName($output) {
		$regexAlias = '/extends \\\(?<alias>\S+)/';

		preg_match($regexAlias, $output, $matches)
			&& preg_match('/use .* as ' . $matches['alias'] . '/', $output)
			&& $output = preg_replace($regexAlias, 'extends $1', $output);

		return $output;
	}
}
