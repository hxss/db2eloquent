<?php

namespace DB2Eloquent\Generator;

use DB2Eloquent\DBInfo\Postgresql\ModelInfo;
use DB2Eloquent\Interfaces\InterfaceContext;
use DB2Eloquent\Interfaces\InterfaceGenerator;
use DB2Eloquent\Traits\LazyProperty;

abstract class ModelGenerator implements InterfaceGenerator {

	use LazyProperty;

	private $class = null;
	protected $modelInfo = null;
	protected $context = null;

	public static function concatNamespace($namespace1, $namespace2) {
		return implode('\\', array_filter(func_get_args()));
	}

	public function __construct(InterfaceContext $context, ModelInfo $modelInfo) {
		$this->modelInfo = $modelInfo;
		$this->context = $context;
	}

	abstract public function generate();

	protected function getClass() {
		return $this->lazyProp('class', function() {
			return (new ClassGenerator())
				->setIndentation("\t")
				->setEgyptianBrackets(true);
		});
	}
}
