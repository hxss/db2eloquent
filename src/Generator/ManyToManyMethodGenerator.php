<?php

namespace DB2Eloquent\Generator;

class ManyToManyMethodGenerator extends RelationMethodGenerator {

	const BODY = "return \$this->belongsToMany(\n\t%s::class,\n\t%s,\n\t%s,\n\t%s\n);";

	protected $pivotModel = null;

	public function generate() {
		if ($this->getForeignModel() && $this->getPivotModel()) {

			($this->getForeignModel() != $this->modelInfo)
				&& $this->uses[] = [
					$this->getForeignModel()->getFullClassName(),
					$this->getChildAlias($this->getForeignModel())
				];

			($this->getPivotModel() != $this->modelInfo)
				&& $this->uses[] = [
					$this->getPivotModel()->getFullClassName(),
					$this->getChildAlias($this->getPivotModel())
				];

			$this->getMethod()
				->setBody($this->getBody());

			return $this->getMethod();
		}

		return null;
	}

	public function __toString() {
		return $this->modelInfo->getTable()
			. ' ~ '
			. $this->getPivotModel()->getTable()
			. '.' . $this->link->getKeys()[0]
			. '/' . $this->link->getForeignKeys()[0]
			. ' ~ '
			. $this->getForeignModel()->getTable();
	}

	protected function getBody() {
		return sprintf(
			static::BODY,
			$this->getChildAlias($this->getForeignModel()),
			$this->getPivotTable(),
			$this->getKey(),
			$this->getForeignKey()
		);
	}

	protected function getPivotModel() {
		return $this->lazyProp('pivotModel', function() {
			$modelsList = $this
				->context
				->getModelsList();
			$pivotTable = $this->link->getPivotTable();

			return (isset($modelsList[$pivotTable]))
				? $modelsList[$pivotTable]
				: null;
		});
	}

	protected function getPivotTable() {
		return $this->getPivotIncluded()
			? $this->getChildAlias($this->getPivotModel()) . '::TABLE'
			: "'" . $this->getPivotModel()->getFullTableName() . "'";
	}

	protected function getKey() {
		return $this->lazyProp('key', function() {
			return $this->getPivotIncluded()
				? $this->getKeyConst()
				: $this->getKeyColumn();
		});
	}

	protected function getKeyConst() {
		return $this->getRelationKey(
			$this->getPivotModel(),
			$this->link->getKeys()[0]
		);
	}

	protected function getKeyColumn() {
		return "'" . $this->link->getKeys()[0] . "'";
	}

	protected function getForeignKey() {
		return $this->lazyProp('foreignKey', function() {
			return $this->getPivotIncluded()
				? $this->getForeignKeyConst()
				: $this->getForeignKeyColumn();
		});
	}

	protected function getForeignKeyConst() {
		return $this->getRelationKey(
			$this->getPivotModel(),
			$this->link->getForeignKeys()[0]
		);
	}

	protected function getForeignKeyColumn() {
		return "'" . $this->link->getForeignKeys()[0] . "'";
	}

	protected function getPivotIncluded() {
		return $this->getPivotModel()->isIncluded();
	}

	protected function getFqMethodName() {
		return $this->lazyProp('fqMethodName', function() {
			$key = $this->getPivotModel()->getProperties()[
				$this->link->getKeys()[0]
			];
			$fk = $this->getPivotModel()->getProperties()[
				$this->link->getForeignKeys()[0]
			];

			return $this->getMethodName()
				. 'Over' . ucfirst($this->getPivotModel()->getClassName())
				. 'By' . ucfirst($key->getName())
				. ucfirst($fk->getName());
		});
	}
}
