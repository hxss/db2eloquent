<?php

namespace DB2Eloquent\Generator;

use DB2Eloquent\DBInfo\Postgresql\ModelInfo;
use DB2Eloquent\Inflector;
use DB2Eloquent\Interfaces\InterfaceContext;
use DB2Eloquent\Interfaces\InterfaceGenerator;
use DB2Eloquent\Link\Link;
use DB2Eloquent\Traits\LazyProperty;
use Zend\Code\Generator\MethodGenerator;

abstract class RelationMethodGenerator implements InterfaceGenerator {

	use LazyProperty;

	const CHILD_ALIAS_PREFIX = 'Child';

	protected $context = null;

	protected $key = null;
	protected $modelInfo = null;

	protected $link = null;

	protected $uses = [];

	protected $foreignKey = null;
	protected $foreignModel = null;

	protected $methodName = null;
	protected $fqMethodName = null;

	protected $useFqn = false;

	private $method = null;

	public function __construct(
		InterfaceContext $context,
		ModelInfo $modelInfo,
		Link $link
	) {
		$this->context = $context;
		$this->modelInfo = $modelInfo;
		$this->link = $link;
	}

	public function getUses() {
		return $this->uses;
	}

	public function setUseFqn($use) {
		$this->useFqn = $use;

		return $this;
	}

	public function getMethodName() {
		return $this->getForeignModel()
			? $this->lazyProp('methodName', function() {
				$methodName = lcfirst(
					$this->getForeignModel()
						->getClassName()
				);

				$this->link->isParent()
					&& $methodName = Inflector::get()->pluralize($methodName);

				return $methodName;
			})
			: null;
	}

	abstract public function __toString();

	abstract protected function getFqMethodName();

	protected function fillMethodName() {
		$this->getMethod()
			->setName(
				$this->useFqn
					? $this->getFqMethodName()
					: $this->getMethodName()
			);
	}

	protected function getMethod() {
		if ($this->method === null) {
			$this->method = new MethodGenerator();
			$this->fillMethodName();
		}

		return $this->method;
	}

	protected function getRelationKey(ModelInfo $modelInfo, $key) {
		$columns = $modelInfo->getColumns();
		$class = $this->modelInfo == $modelInfo
			? 'self'
			: static::CHILD_ALIAS_PREFIX . $modelInfo->getClassName();

		return isset($columns[$key])
			? $class . '::' . $columns[$key]->getConstant()
			: $key;
	}

	protected function getChildAlias(ModelInfo $modelInfo) {
		return $modelInfo == $this->modelInfo
			? 'static'
			: static::CHILD_ALIAS_PREFIX . $modelInfo->getClassName();
	}

	protected function getForeignModel() {
		return $this->lazyProp('foreignModel', function() {
			$modelsList = $this
				->context
				->getModelsList();
			$foreignTable = $this->link->getForeignTable();

			return (
				isset($modelsList[$foreignTable])
				&& $modelsList[$foreignTable]->isIncluded()
			)
				? $modelsList[$foreignTable]
				: null;
		});
	}
}
