<?php

namespace DB2Eloquent\Interfaces;

interface InterfaceContext {

	public function getInput();

	public function getOutput();

	public function getConfig();

	public function getModelsList();
}
