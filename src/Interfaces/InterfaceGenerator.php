<?php

namespace DB2Eloquent\Interfaces;

interface InterfaceGenerator {

	public function generate();
}
