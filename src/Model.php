<?php

namespace DB2Eloquent;

use Illuminate\Database\Eloquent\Model as IlluminateModel;

class Model extends IlluminateModel {

	public function getAttribute($key) {
		return parent::getAttribute($key)
			?: parent::getAttribute(snake_case($key));
	}

	public function setAttribute($key, $value) {
		return parent::setAttribute(snake_case($key), $value);
	}

	public function __call($method, $args) {
		if (preg_match('/set(?<attr>[A-Z]\w+)/', $method, $matches)) {
			return $this->setAttribute($matches['attr'], ...$args);
		}

		if (preg_match('/get(?<attr>[A-Z]\w+)/', $method, $matches)) {
			return $this->getAttribute($matches['attr'], ...$args);
		}

		return parent::__call($method, $args);
	}
}
