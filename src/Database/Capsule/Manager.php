<?php

namespace DB2Eloquent\Database\Capsule;

use DB2Eloquent\Database\DatabaseManager;
use Illuminate\Database\Capsule\Manager as IlluminateManager;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\PostgresConnection;

class Manager extends IlluminateManager {

	public function __construct() {
		parent::__construct(...func_get_args());

		$this->manager->extend('pgsql', function($config, $name) {
			return isset($config['pdo'])
				? new PostgresConnection($config['pdo'])
				: $this->manager->getFactory()->make($config, $name);
		});
	}

	protected function setupManager() {
		$factory = new ConnectionFactory($this->container);

		$this->manager = new DatabaseManager($this->container, $factory);
	}
}
