SELECT
	parent_ns.nspname parent_schema,
	parent_cl.relname parent_table,
	array_to_json(array(
		select parent_attr.attname parent_key
		from pg_attribute parent_attr
		where parent_attr.attrelid = parent_cl.oid
			and parent_attr.attnum = any(pg_constraint.confkey)
	)) parent_attrs,

	child_ns.nspname child_schema,
	child_cl.relname child_table,
	array_to_json(array(
		select child_attr.attname foreign_key
		from pg_attribute child_attr
		where child_attr.attrelid = child_cl.oid
			and child_attr.attnum = any(pg_constraint.conkey)
	)) child_attrs
FROM pg_constraint
	join pg_class child_cl on (child_cl.oid = pg_constraint.conrelid)
		join pg_namespace child_ns on (child_ns.oid = child_cl.relnamespace)
	join pg_class parent_cl on (parent_cl.oid = pg_constraint.confrelid)
		join pg_namespace parent_ns on (parent_ns.oid = parent_cl.relnamespace)
where pg_constraint .contype = 'f'
	and (
		(
			parent_ns.nspname = :schema
			and parent_cl.relname = :table
		) or (
			child_ns.nspname = :schema
			and child_cl.relname = :table
		)
	)
